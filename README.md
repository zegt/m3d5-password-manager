M3D5 Password Manager
======
###Introduction
M3D5 Password Manager is a just another password manager that allows you to collect your personal data into a txt files hashed in MD5 with a personal key.

![m3d5passwordmanager](http://i.imgur.com/CaaUQrv.jpg)

###Download and installation
1) Download from 
```
https://github.com/zegt/m3d5-password-manager/releases
```
2) Install running setup.exe

###Features
- Hash your password in MD5 using a personal key.
- Developed in vb.NET.
- Available only on Microsoft Windows.
- Easy, free and open source.

###License
M3D5 Password Manager is published under GNU General Public License v3.0.
Project started by Alessandro Celso (@zegt) in 2015.
